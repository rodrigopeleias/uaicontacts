package uaiContacts.vo;

import java.util.List;

import uaiContacts.model.Contact;

public class ContactListVO {
	
	private int pagesCount;
	private long totalCounts;
	
	private String actionMessage;
	private String searchMessage;
	
	private List<Contact> contacts;
	
	public ContactListVO() {
	}
	
	public ContactListVO(int pages, long totalContacts, List<Contact> contacts) {
		this.pagesCount = pages;
		this.contacts = contacts;
		this.totalCounts = totalContacts;
	}

	public int getPagesCount() {
		return pagesCount;
	}

	public void setPagesCount(int pagesCount) {
		this.pagesCount = pagesCount;
	}

	public long getTotalCounts() {
		return totalCounts;
	}

	public void setTotalCounts(long totalCounts) {
		this.totalCounts = totalCounts;
	}

	public String getActionMessage() {
		return actionMessage;
	}

	public void setActionMessage(String actionMessage) {
		this.actionMessage = actionMessage;
	}

	public String getSearchMessage() {
		return searchMessage;
	}

	public void setSearchMessage(String searchMessage) {
		this.searchMessage = searchMessage;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}	
}
